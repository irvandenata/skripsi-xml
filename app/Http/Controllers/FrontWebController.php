<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;

class FrontWebController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['breadcrumb'] = "Website Nama";
        return view('frontwebsite.landing',$data);
    }

    public function produk (){
        $data['breadcrumb'] = "Produk";

        $data['produk']  = Produk::orderBy('nama')->get();
    return view('frontwebsite.produk',$data);
    }
    public function produkDetail ($id){
        $data['breadcrumb'] = "Produk Detail";
        $data['produk']  = Produk::where('id',$id)->first();
        return view('frontwebsite.produk-detail',$data);
    }
}
