<?php

namespace App\Http\Controllers;

use App\Models\BrandEquipment;
use App\Models\CategoryEquipment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiDataController extends Controller
{
    public function getCategoriesEquipment(Request $request){
        $result = null;
        if($request->data != '' && $request->data != null && strlen($request->data) > 2){
            $result = CategoryEquipment::where('name','like','%'.$request->data.'%')->orderBy('name')->get();
        }
        return $result;
    }

    public function getBrandsEquipment(Request $request){
        $result = null;
        if($request->data != '' && $request->data != null && strlen($request->data) > 2){
            $result = BrandEquipment::where('name','like','%'.$request->data.'%')->orderBy('name')->get();
        }
        return $result;
    }
    public function createBrand (Request $request){
        DB::beginTransaction();
        $brand = BrandEquipment::where('name',$request->name)->first();
        if($brand){
            return response()->json(['message' => "Brand has available !", "data" => ''], 200);
        }
        try {
            $item = new BrandEquipment();
            $item->name = $request->name;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e, 500);
        }
        return response()->json(['message' => "Brand has been created !", "data" => $item], 200);
    }
    public function createCategory (Request $request){
        DB::beginTransaction();
        $category = CategoryEquipment::where('name',$request->name)->first();
        if($category){
            return response()->json(['message' => "Category has available !", "data" => ''], 200);
        }
        try {
            $item = new CategoryEquipment();
            $item->name = $request->name;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e, 500);
        }
        return response()->json(['message' => "Category has been created !", "data" => $item], 200);
    }
}
