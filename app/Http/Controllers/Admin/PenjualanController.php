<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Penjualan;
use App\Models\Produk;
use App\Models\Setting;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PenjualanController extends Controller
{
    protected $name = 'Penjualan';
    protected $breadcrumb = '<strong>Data</strong> Penjualan';
    protected $modul = 'penjualan';
    protected $route = 'penjualans';
    protected $view = 'admin.penjualan';

    public function __construct()
    {
        $this->newModel = new Penjualan();
        $this->model = Penjualan::query();
        $this->rows = [
            'name'=>['Kode','Nama Pembeli','Produk','Jumlah','Tanggal','Status','Sumber'],
            'column' => ['kode','pembeli','produk','jumlah','tanggal','status','sumber']
        ];
        $this->createLink = route('admin.penjualans.create');
        $this->storeLink = route('admin.penjualans.store');
        $this->indexLink = route('admin.penjualans.index');
        $this->updateLink = 'admin.penjualans.update';
        $this->editLink = 'admin.penjualans.edit';
    }

    protected static function validateRequest($request, $type)
    {
        if ($type == 'create') {
            $result = Validator::make($request->all(), [
                'pembeli' => 'required',
                'kode' => 'required',
            ]);
        } else {
            $result = Validator::make($request->all(), [
                'pembeli' => 'required',
                'kode' => 'required',

            ]);
        }
        return $result;
    }

    protected function findById($id)
    {
        $model = clone $this->model;
        return $model->where('id', $id)->first();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->model->latest()->get();
            return DataTables::of($items)
                ->addColumn('action', function ($item) {
                    if(auth()->user()->is_admin){
                        return '
                           <a class="btn btn-danger btn-sm"  onclick="deleteItem(' . $item->id . ')"><i class="fas fa-trash text-white"></i></span></a> <a class="btn btn-warning btn-sm" onclick="editItem('.$item->id.')" ><i class="fas fa-pencil text-white    "></i></span></a>';
                    } else return 'No Action';
                })
                ->editColumn('status', function ($item) {
                    return $item->status == 1 ? '<span class="badge bg-primary">Sinkron</span>' : '<span class="badge bg-secondary">Tidak Sinkron</span>';
                })
                ->addColumn('produk', function ($item) {
                    return $item->produk->nama."(".$item->produk->kode.")" ;
                })
                ->removeColumn('id')
                ->addIndexColumn()
                ->rawColumns(['action','status'])
                ->make(true);
        }
        $data['title'] = $this->name;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['rows'] = $this->rows;
        $data['produks'] = Produk::orderBy('nama')->get();
        $data['createLink'] = $this->createLink;
        $data['view'] = $this->view;
        return view($this->view.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = $this->name . ' - Create';
        $data['breadcrumb'] =  $this->breadcrumb . ' - Create';
        $data['storeLink'] = $this->storeLink;
        $data['indexLink'] = $this->indexLink;
        return view($this->view.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'create');
            if ($v->fails()) {
                throw new CustomException("error", 401, null, $v->errors()->all());
            }

            $item = $this->newModel;
            $item->pembeli = $request->pembeli;
            $item->tanggal = $request->tanggal;
            $item->jumlah = $request->jumlah;
            $item->sumber = $request->sumber;
            $item->produk_id = $request->produk_id;
            $item->kode = $request->kode;
            $item->status = 0;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            DB::rollback();
            return response()->json($e->getOptions(),$e->getCode());
        }
        return response()->json(['message' => "$this->name berhasil di buat !", "data" => $item], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        return $penjualan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'edit');
            if ($v->fails()) {
                throw new CustomException('error', 401, null, $v->errors()->all());
            }
            $item = $this->findById($id);
            $item->pembeli = $request->pembeli;
            $item->tanggal = $request->tanggal;
            $item->jumlah = $request->jumlah;
            $item->sumber = $request->sumber;
            $item->produk_id = $request->produk_id;
            $item->kode = $request->kode;
            $item->status = 0;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            DB::rollback();
            return response()->json($e->getOptions(),$e->getCode());
        }
        return response()->json(['message' => "$this->name berhasil di perbaharui !", "data" => $item], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->findById($id);
            if(!$item){
                throw new CustomException("error", 404, null, ["Data not found"]);
            }
            $item->delete();
            return response()->json(['message' => "$this->name berhasil dihapus !"], 200);

        }catch (Exception $e) {
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
    }


    public function sinkron()
    {
        try {
            if(Penjualan::get()->count()>1) Penjualan::query()->where('sumber', request()->sheat)->update(['status'=>0]);
            $link = Setting::where('config_name', request()->sheat)->first();
            $linkApi = Setting::where('config_name', 'link_api')->first()->config_value;
            $fullLink = "$linkApi?id=$link->config_value&tipe=penjualan";
            $xmlfile = file_get_contents($fullLink);
            $new = simplexml_load_string($xmlfile);
            $con = json_encode($new);
            $newArr = json_decode($con, true);
            foreach ($newArr as $key => $data) {
            $month = $key;
              foreach ($data['row'] as $key => $item) {
                if($key ==0) continue;
                if(!isset($item['cell'][5]))dd($item['cell']);
                $date = explode(' ',$item['cell'][1]);
                $date = $date[2].' '.$date[1].' '.$date[3];
                $date = date('Y-m-d',strtotime($date));
                $penjualan = clone $this->model;
                $penjualan = $penjualan->where('kode', $item['cell'][5])->first();
                $produkId = Produk::where('nama',$item['cell'][3])->first();
                if(!$produkId){
                    continue;
                }
                if (!$penjualan) {
                    Penjualan::create([
                        'pembeli'=>$item['cell'][2],
                        'tanggal'=>$date,
                        'kode'=>$item['cell'][5],
                        'produk_id'=>$produkId->id,
                        'status'=>1,
                        'sumber'=>$link->name,
                        'jumlah'=>$item['cell'][4],
                    ]);
                }else{
                    $penjualan->pembeli = $item['cell'][2];
                    $penjualan->tanggal = $date;
                    $penjualan->kode = $item['cell'][5];
                    $penjualan->status = 1;
                    $penjualan->produk_id = $produkId->id;
                    $penjualan->sumber = $link->name;
                    $penjualan->jumlah = $item['cell'][4];
                    $penjualan->save();
                }
            }
        }
        } catch (Exception $e) {
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
        return response()->json(['message' => "success"], 200);
    }


}
