<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Produk;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProdukController extends Controller
{
    protected $name = 'Produk';
    protected $breadcrumb = '<strong>Data</strong> Produk';
    protected $modul = 'produk';
    protected $route = 'produks';
    protected $view = 'admin.produk';

    public function __construct()
    {
        $this->newModel = new Produk();
        $this->model = Produk::query();
        $this->rows = [
            'name' => ['Code', 'Nama', 'Stok','Harga','Gambar', 'Status'],
            'column' => ['kode', 'nama', 'stok','harga','gambar', 'status'],
        ];
        $this->createLink = route('admin.produks.create');
        $this->storeLink = route('admin.produks.store');
        $this->indexLink = route('admin.produks.index');
        $this->updateLink = 'admin.produks.update';
        $this->editLink = 'admin.produks.edit';
    }

    protected static function validateRequest($request, $type)
    {
        if ($type == 'create') {
            $result = Validator::make($request->all(), [
                'nama' => 'required',
                'kode' => 'required',
                'stok' => 'required',
            ]);
        } else {
            $result = Validator::make($request->all(), [
                'nama' => 'required',
                'kode' => 'required',
                'stok' => 'required',
            ]);
        }
        return $result;
    }

    protected function findById($id)
    {
        $model = clone $this->model;
        return $model->where('id', $id)->first();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->model->latest()->get();
            return DataTables::of($items)
                ->addColumn('action', function ($item) {
                    return '
                           <a class="btn btn-danger btn-sm"  onclick="deleteItem(' . $item->id . ')"><i class="fas fa-trash text-white"></i></span></a> <a class="btn btn-warning btn-sm" onclick="editItem(' . $item->id . ')" ><i class="fas fa-pencil text-white    "></i></span></a>';
                })
                ->editColumn('status', function ($item) {
                    return $item->status == 1 ? '<span class="badge bg-primary">Sinkron</span>' : '<span class="badge bg-secondary">Tidak Sinkron</span>';
                })
                ->editColumn('gambar', function ($item) {
                    return $item->gambar?"<img style='width:100px' src='/storage/".$item->gambar."'>":"-";
                })
                ->removeColumn('id')
                ->addIndexColumn()
                ->rawColumns(['action', 'status','gambar'])
                ->make(true);
        }
        $data['title'] = $this->name;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['rows'] = $this->rows;
        $data['createLink'] = $this->createLink;
        $data['view'] = $this->view;
        return view($this->view . '.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = $this->name . ' - Create';
        $data['breadcrumb'] = $this->breadcrumb . ' - Create';
        $data['storeLink'] = $this->storeLink;
        $data['indexLink'] = $this->indexLink;
        return view($this->view . '.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'create');
            if ($v->fails()) {
                throw new CustomException("error", 401, null, $v->errors()->all());
            }
            $item = $this->newModel;
            $item->nama = $request->nama;
            $item->stok = $request->stok;
            $item->deskripsi = $request->deskripsi;
            if($request->gambar){
                $item->gambar = GlobalHelper::storeSingleImage($request->gambar,'produk');
            }
            $item->kode = $request->kode;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            DB::rollback();
            return response()->json($e->getOptions(), $e->getCode());
        }
        return response()->json(['message' => "$this->name berhasil dibuat !", "data" => $item], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        return $produk;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'edit');
            if ($v->fails()) {
                throw new CustomException('error', 401, null, $v->errors()->all());
            }
            $item = $this->findById($id);
            $item->nama = $request->nama;
            $item->stok = $request->stok;
            $item->kode = $request->kode;
            $item->deskripsi = $request->deskripsi;
            if($request->gambar){
                if($item->gambar)GlobalHelper::deleteSingleImage($item->gambar);
                $item->gambar = GlobalHelper::storeSingleImage($request->gambar,'produk');
            }
            $item->status = 0;
            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            DB::rollback();
            return response()->json($e->getOptions(), $e->getCode());
        }
        return response()->json(['message' => "$this->name berhasil di perbaharui !", "data" => $item], 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->findById($id);
            if (!$item) {
                throw new CustomException("error", 404, null, ["Data not found"]);
            }
            $item->delete();
            return response()->json(['message' => "$this->name berhasil di hapus !"], 200);
        } catch (Exception $e) {
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
    }

    public function sinkron()
    {
        try {
            $link = Setting::where('config_name', 'sheat_produk')->first();
            $linkApi = Setting::where('config_name', 'link_api')->first()->config_value;
            $fullLink = "$linkApi?id=$link->config_value&tipe=produk";
            $xmlfile = file_get_contents($fullLink);
            $new = simplexml_load_string($xmlfile);
            $con = json_encode($new);
            $newArr = json_decode($con, true);
            $oldData = Produk::query()->update([
                'status'=>0
            ]);
            foreach ($newArr['row'] as $key => $item) {
                if($key ==0) continue;
                unset($produk);
                $produk = clone $this->model;
                $produk = $produk->where('kode', $item['cell'][2])->first();
                if (!$produk) {
                    Produk::create([
                        'nama'=>$item['cell'][0],
                        'stok'=>$item['cell'][1],
                        'kode'=>$item['cell'][2],
                        'harga'=>$item['cell'][3],
                        'status'=>1,
                    ]);
                }else{
                    $produk->nama = $item['cell'][0];
                    $produk->stok = $item['cell'][1];
                    $produk->kode = $item['cell'][2];
                    $produk->harga = $item['cell'][3];
                    $produk->status = 1;
                    $produk->save();
                }
            }
        } catch (Exception $e) {
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
        return response()->json(['message' => "success"], 200);
    }
}
