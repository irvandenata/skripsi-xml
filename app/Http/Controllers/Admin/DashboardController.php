<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Penjualan;
use App\Models\Produk;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['breadcrumb'] = 'Dashboard';
        $data['title'] = 'Dashboard';
        $year = date('Y');
        $month = date('m');
        $lastYear = $year;
        $lastMonth = date("m",strtotime("-1 month"));
        if($lastMonth=='12'){
            $lastYear = strval(intval($year)-1);
        }

        $data['pj_bulan'] = Penjualan::where('status',1)->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get()->sum('jumlah');

        $omset = Penjualan::where('status',1)->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();

        $omsetYoutube = Penjualan::where('status',1)->where('sumber','Youtube')->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();
        $data['omset_youtube'] = 0;
        foreach ($omsetYoutube as $key => $item) {
            $data['omset_youtube'] += ($item->jumlah * $item->produk->harga );
        }

        $omsetLastYoutube = Penjualan::where('status',1)->where('sumber','Youtube')->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();

        $data['omset_youtube_lalu'] = 0;
        foreach ($omsetLastYoutube as $key => $item) {
            $data['omset_youtube_lalu'] += ($item->jumlah * $item->produk->harga );
        }

        //Omset facebook
        $omsetFacebook = Penjualan::where('status',1)->where('sumber','Facebook')->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();
        $data['omset_facebook'] = 0;
        foreach ($omsetFacebook as $key => $item) {
            $data['omset_facebook'] += ($item->jumlah * $item->produk->harga );
        }

        $omsetLastFacebook = Penjualan::where('status',1)->where('sumber','Facebook')->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();

        $data['omset_facebook_lalu'] = 0;
        foreach ($omsetLastFacebook as $key => $item) {
            $data['omset_facebook_lalu'] += ($item->jumlah * $item->produk->harga );
        }

        //akram ads
        $omsetAkram = Penjualan::where('status',1)->where('sumber','Akram Ads')->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();
        $data['omset_akram'] = 0;
        foreach ($omsetAkram as $key => $item) {
            $data['omset_akram'] += ($item->jumlah * $item->produk->harga );
        }

        $omsetLastAkram = Penjualan::where('status',1)->where('sumber','Akram Ads')->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();

        $data['omset_akram_lalu'] = 0;
        foreach ($omsetLastAkram as $key => $item) {
            $data['omset_akram_lalu'] += ($item->jumlah * $item->produk->harga );
        }

        //Instagram
        $omsetInstagram = Penjualan::where('status',1)->where('sumber','Instagram')->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();
        $data['omset_instagram'] = 0;
        foreach ($omsetInstagram as $key => $item) {
            $data['omset_instagram'] += ($item->jumlah * $item->produk->harga );
        }

        $omsetLastInstagram = Penjualan::where('status',1)->where('sumber','Instagram')->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();

        $data['omset_instagram_lalu'] = 0;
        foreach ($omsetLastInstagram as $key => $item) {
            $data['omset_instagram_lalu'] += ($item->jumlah * $item->produk->harga );
        }

        //market place
        $omsetMarket = Penjualan::where('status',1)->where('sumber','Market Place')->whereYear('tanggal', '=', $year)
        ->whereMonth('tanggal', '=', $month)
        ->get();
        $data['omset_market'] = 0;
        foreach ($omsetMarket as $key => $item) {
            $data['omset_market'] += ($item->jumlah * $item->produk->harga );
        }


        $omsetLastMarket = Penjualan::where('status',1)->where('sumber','Market Place')->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();



        $data['omset_market_lalu'] = 0;
        foreach ($omsetLastMarket as $key => $item) {
            $data['omset_market_lalu'] += ($item->jumlah * $item->produk->harga );
        }




        $data['omset_bulan'] = 0;
        foreach ($omset as $key => $item) {
            $data['omset_bulan'] += ($item->jumlah * $item->produk->harga );
        }


        $data['pj_bulan_lalu'] = Penjualan::where('status',1)->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get()->sum('jumlah');

        $omset_lalu = Penjualan::where('status',1)->whereYear('tanggal', '=', $lastYear)
        ->whereMonth('tanggal', '=', $lastMonth)
        ->get();
        $data['omset_bulan_lalu'] = 0;
        foreach ($omset_lalu as $key => $item) {
            $data['omset_bulan_lalu'] += ($item->jumlah * $item->produk->harga );
        }

        $data['produk'] = Produk::get()->count();
        $data['produkTerbanyak'] =  Penjualan::whereYear('tanggal', '=', $year) ->whereMonth('tanggal', '=', $month)->selectRaw("SUM(jumlah) as total_jual,produks.nama")->leftJoin('produks','produk_id','produks.id')
            ->where('penjualans.status',1)
            ->groupBy('produk_id')
            ->orderBy('total_jual','desc')
            ->get();

        $data['perjualanPerBulan'] = Penjualan::whereYear('tanggal', '=', $year)
        ->leftJoin('produks','produk_id','produks.id')
        ->select(DB::raw('MONTH(tanggal) month , SUM(jumlah) as total_jual,produks.nama'))
        ->where('penjualans.status',1)
        ->orderBy('produks.nama')
        ->orderBy('month')
        ->groupBy('produk_id','month')
        ->get()->toArray();

        foreach ($data['perjualanPerBulan'] as $key => $item) {
            if(!isset($test[$item['nama']])){
                $test[$item['nama']]=[
                   [ $item['month'],$item['total_jual']]
                ];
            }else{
                array_push($test[$item['nama']],[$item['month'], $item['total_jual']]);
            }
        }

        $data['perjualanPerBulan'] = $test;
        return view('dashboard',$data);
    }

    public function updateProfile (Request $request){
        DB::beginTransaction();
        try {
            $user = User::where('id',auth()->user()->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
        return response()->json(['message' => "Profile has been updated !", "data" => '$item'], 200);
    }
}
