<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class SettingController extends Controller
{
    protected $name = 'Setting';
    protected $breadcrumb = '<strong>Halaman</strong> Setting';
    protected $modul = 'setting';
    protected $route = 'settings';
    protected $view = 'admin.setting';

    public function __construct()
    {
        $this->newModel = new Setting();
        $this->model = Setting::query();
        $this->createLink = route('admin.settings.create');
        $this->storeLink = route('admin.settings.store');
        $this->indexLink = route('admin.settings.index');
        $this->updateLink = 'admin.settings.update';
        $this->editLink = 'admin.settings.edit';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['sheat_youtube'] = clone $this->model;
        $data['sheat_youtube'] = $data['sheat_youtube']->where('config_name','sheat_youtube')->first();
        $data['link_api'] = clone $this->model;
        $data['link_api'] = $data['link_api']->where('config_name','link_api')->first();
        $data['sheat_ads'] = clone $this->model;
        $data['sheat_ads'] = $data['sheat_ads']->where('config_name','sheat_ads')->first();
        $data['sheat_facebook'] = clone $this->model;
        $data['sheat_facebook'] = $data['sheat_facebook']->where('config_name','sheat_facebook')->first();
        $data['sheat_produk'] = clone $this->model;
        $data['sheat_produk'] = $data['sheat_produk']->where('config_name','sheat_produk')->first();
        $data['sheat_instagram'] = clone $this->model;
        $data['sheat_instagram'] = $data['sheat_instagram']->where('config_name','sheat_instagram')->first();
        $data['sheat_market'] = clone $this->model;
        $data['sheat_market'] = $data['sheat_market']->where('config_name','sheat_market')->first();
        $data['title'] = $this->name;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['createLink'] = $this->createLink;
        $data['view'] = $this->view;
        return view($this->view.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $link = $request->link;
            $linkApi = Setting::where('config_name','link_api')->first();
            $linkApi->config_value = $link;
            $linkApi->save();
            $sheatYoutube = Setting::where('config_name','sheat_youtube')->first();
            $sheatYoutube->config_value = $request->sheat_youtube;
            $sheatYoutube->save();
            $sheatAds = Setting::where('config_name','sheat_ads')->first();
            $sheatAds->config_value = $request->sheat_ads;
            $sheatAds->save();
            $sheatFacebook = Setting::where('config_name','sheat_facebook')->first();
            $sheatFacebook->config_value = $request->sheat_facebook;
            $sheatFacebook->save();
            $sheatInstagram = Setting::where('config_name','sheat_instagram')->first();
            $sheatInstagram->config_value = $request->sheat_instagram;
            $sheatInstagram->save();
            $sheatMarket = Setting::where('config_name','sheat_market')->first();
            $sheatMarket->config_value = $request->sheat_market;
            $sheatMarket->save();
            $sheatProduk = Setting::where('config_name','sheat_produk')->first();
            $sheatProduk->config_value = $request->sheat_produk;
            $sheatProduk->save();
            return redirect($this->indexLink)->with('success', 'Data Berhasil di update');
        } catch (Exception $e) {
            dd($e);
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return $setting;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'edit');
            if ($v->fails()) {
                throw new CustomException('error', 401, null, $v->errors()->all());
            }
            $item = $this->findById($id);
            $item->name = $request->name;
            $item->location = $request->location;
            $item->save();
            DB::commit();
            return redirect($this->indexLink)->with('success', 'Data has been updated');
        } catch (Exception $e) {
            DB::rollback();
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            DB::rollback();
            return response()->json($e->getOptions(),$e->getCode());
        }
        return response()->json(['message' => "$this->name has been created !", "data" => $item], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->findById($id);
            if(!$item){
                throw new CustomException("error", 404, null, ["Data not found"]);
            }
            $item->delete();
            return response()->json(['message' => "$this->name has been deleted !"], 200);

        }catch (Exception $e) {
            return response()->json($e->getOptions(), 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
    }

}
