<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Equipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class EquipmentController extends Controller
{
    protected $name = 'Data Equipment';
    protected $breadcrumb = '<strong>Data</strong> Equipment';
    protected $modul = 'equipment';
    protected $route = 'equipments';
    protected $view = 'admin.equipment';

    public function __construct()
    {
        $this->newModel = new Equipment();
        $this->model = Equipment::query();
        $this->rows = [
            'name'=>['Name','Type','Category','Brand'],
            'column' => ['name','type','brand','category',]
        ];
        $this->createLink = route('admin.equipments.create');
        $this->storeLink = route('admin.equipments.store');
        $this->indexLink = route('admin.equipments.index');
        $this->updateLink = 'admin.equipments.update';
        $this->editLink = 'admin.equipments.edit';
    }

    protected static function validateRequest($request, $type)
    {
        if ($type == 'create') {
            $result = Validator::make($request->all(), [
                'category_id' => 'required',
                'brand_id' => 'required',
                'name'=> 'required',
                'type'=> 'required',
            ]);
        } else {
            $result = Validator::make($request->all(), [
                'category_id' => 'required',
                'brand_id' => 'required',
                'name'=> 'required',
                'type'=> 'required',
            ]);
        }

        return $result;
    }
    protected function findById($id)
    {
        $model = clone $this->model;
        return $model->where('id', $id)->first();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->model->latest();
            return DataTables::of($items)
                ->addColumn('action', function ($item) {
                    return '
                           <a class="btn btn-danger btn-sm"  onclick="deleteItem(' . $item->id . ')"><i class="fas fa-trash text-white"></i></span></a> <a class="btn btn-warning btn-sm" href="' .route($this->editLink,$item->id) . '" ><i class="fas fa-pencil text-white    "></i></span></a>';
                })
                ->addColumn('category',function($item){
                    return $item->category->name;
                })
                ->addColumn('brand',function($item){
                    return $item->brand->name;
                })
                ->removeColumn('id')
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $data['title'] = $this->name;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['rows'] = $this->rows;
        $data['createLink'] = $this->createLink;
        return view($this->view.'.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = $this->name . ' - Create';
        $data['breadcrumb'] =  $this->breadcrumb . ' - Create';
        $data['storeLink'] = $this->storeLink;
        $data['indexLink'] = $this->indexLink;
        return view($this->view.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $v = $this->validateRequest($request, 'create');
            if ($v->fails()) {
                throw new CustomException("error", 401, null, $v->errors()->all());
            }
            $item = $this->newModel;
            $item->category_id = $request->category_id;
            $item->brand_id = $request->brand_id;
            $item->name = $request->name;
            $item->type = $request->type;
            $item->save();
            DB::commit();
            return redirect($this->indexLink)->with('success', 'Data has been created');
        } catch (Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        } catch (CustomException $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error', $e->getOptions());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = $this->name . ' - Edit';
        $data['breadcrumb'] = $this->name . ' - Edit';
        $data['item'] = $this->findById($id);
        $data['updateLink'] = route($this->updateLink,$id);
        $data['indexLink'] = $this->indexLink;
        return view($this->view.'.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $v = $this->validateRequest($request, 'edit');
            if ($v->fails()) {
                throw new CustomException('error', 401, null, $v->errors()->all());
            }
            $item = $this->findById($id);
            $item->category_id = $request->category_id;
            $item->brand_id = $request->brand_id;
            $item->name = $request->name;
            $item->type = $request->type;
            $item->save();
            DB::commit();
            return redirect($this->indexLink)->with('success', 'Data has been updated');
        } catch (Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        } catch (CustomException $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with('error', $e->getOptions());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->findById($id);
            if (!$item) {
                throw new CustomException("error", 404, null, ["Data not found"]);
            }
            $item->delete();
            return response()->json(['message' => "$this->name has been deleted !"], 200);
        } catch (Exception $e) {
            return response()->json($e, 500);
        } catch (CustomException $e) {
            return response()->json($e->getOptions(), 500);
        }
    }

}
