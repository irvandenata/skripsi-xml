<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryEquipment extends Model
{
    use HasFactory;
    protected $table = 'category_equipments';
    public function equipments(){
        return $this->hasMany(Inventory::class,'category_id');
        }
}
