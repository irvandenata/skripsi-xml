<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;
    protected $table = 'equipments';

    public function category (){
    return $this->belongsTo(CategoryEquipment::class,'category_id');
    }
    public function brand (){
        return $this->belongsTo(BrandEquipment::class,'brand_id');
        }
}
