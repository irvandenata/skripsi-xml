<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandEquipment extends Model
{
    use HasFactory;
    protected $table = 'brand_equipments';

    public function inventories(){
        return $this->hasMany(Equipment::class,'brand_id');
        }
}
