<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware'=>'auth','prefix'=>'admin','as'=>'admin.'],function(){
    Route::resource('equipments', App\Http\Controllers\Admin\EquipmentController::class);
    Route::resource('technicians', App\Http\Controllers\Admin\TechnicianController::class);
    Route::resource('rooms', App\Http\Controllers\Admin\RoomController::class);
    Route::resource('settings', App\Http\Controllers\Admin\SettingController::class);
    Route::resource('penjualans', App\Http\Controllers\Admin\PenjualanController::class);
    Route::resource('produks', App\Http\Controllers\Admin\ProdukController::class);
});


Route::get('/api/get-categories-equipment',[App\Http\Controllers\ApiDataController::class, 'getCategoriesEquipment']);
Route::get('/api/get-brands-equipment',[App\Http\Controllers\ApiDataController::class, 'getBrandsEquipment']);
Route::post('/api/create-brand',[App\Http\Controllers\ApiDataController::class, 'createBrand']);
Route::post('/api/create-category',[App\Http\Controllers\ApiDataController::class, 'createCategory']);


Route::get('/xml',function(){
    $xmlfile = file_get_contents('https://script.google.com/macros/s/AKfycbwA0oaHNkkULlCjY34-8TBcD-2yTO6cTR11bn2-zMTmZm4SgLdkuiwXMdVIeypO4zeEhA/exec?id=1jju-d7idlhJLwH58pyWrj2-61R32y87Xu8LiI56JIbQ');
$new = simplexml_load_string($xmlfile);
$con = json_encode($new);
$newArr = json_decode($con, true);
    return $newArr;
});
