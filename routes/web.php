<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\FrontWebController::class,'index']);
Route::get('/produk', [App\Http\Controllers\FrontWebController::class,'produk'])->name('product');
Route::get('/produk-detail/{id}', [App\Http\Controllers\FrontWebController::class,'produkDetail'])->name('product-detail');

Auth::routes();

Route::get('/home',function(){
    return redirect()->route('admin.dashboard.index');
});
Route::group(['middleware'=>'auth','prefix'=>'admin','as'=>'admin.'],function(){
    Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('/grafiks', [App\Http\Controllers\Admin\GrafikController::class, 'index'])->name('grafiks.index');
    Route::post('/send/profile', [App\Http\Controllers\Admin\DashboardController::class, 'updateProfile'])->name('dashboard.update-profile');

    Route::resource('settings', App\Http\Controllers\Admin\SettingController::class);
    Route::get('penjualans/sinkron', [App\Http\Controllers\Admin\PenjualanController::class,'sinkron'])->name('penjualans.sinkron');
    Route::resource('penjualans', App\Http\Controllers\Admin\PenjualanController::class);
    Route::get('produks/sinkron', [App\Http\Controllers\Admin\ProdukController::class,'sinkron'])->name('produks.sinkron');
    Route::resource('produks', App\Http\Controllers\Admin\ProdukController::class);
    Route::resource('users', App\Http\Controllers\Admin\UserController::class);
    Route::resource('users', App\Http\Controllers\Admin\UserController::class);

});


Route::get('/api/get-categories-equipment',[App\Http\Controllers\ApiDataController::class, 'getCategoriesEquipment']);
Route::get('/api/get-brands-equipment',[App\Http\Controllers\ApiDataController::class, 'getBrandsEquipment']);
Route::post('/api/create-brand',[App\Http\Controllers\ApiDataController::class, 'createBrand']);
Route::post('/api/create-category',[App\Http\Controllers\ApiDataController::class, 'createCategory']);


Route::get('/xml',function(){
    $xmlfile = file_get_contents('https://script.google.com/macros/s/AKfycbwA0oaHNkkULlCjY34-8TBcD-2yTO6cTR11bn2-zMTmZm4SgLdkuiwXMdVIeypO4zeEhA/exec?id=1jju-d7idlhJLwH58pyWrj2-61R32y87Xu8LiI56JIbQ');
    $new = simplexml_load_string($xmlfile);
    $con = json_encode($new);
    $newArr = json_decode($con, true);
    return $newArr;
});
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:cache');
    Artisan::call('config:cache');
    return 'Application cache has been cleared';
});

