@extends('layouts.app')
@section('content')
<div class="row my-5">
    <div class="col-12 text-center"><h1>PRODUK KAMI</h1></div>
    @foreach ($produk as $item)
    <div class="col-3">
        <a href="{{ route('product-detail',$item->id) }}">
        <div class="card my-4 p-2" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="{{ $loop->iteration*100 }}" >
            <div class="image">
            @if ($item->gambar)
            <img style="width: 100%" src="storage/{{ $item->gambar }}" alt="">
            @else
            <img style="width: 100%" src="{{ asset('assets\img\img\backgrounds\18.jpg') }}" alt="">
            @endif
            </div>
            <h5 class="text-center mt-3">{{ $item->nama }}</h5>
        </div>
    </a>
    </div>
    @endforeach

</div>
</div>
@endsection

@push('js')

@endpush
