@extends('layouts.app')
@push('css')
<style>
    .header{
font-weight:900;
font-size:48px;
    }

</style>
@endpush
@section('content')
<div class="row my-5">
    <div class="col-6 my-auto"
    data-aos="fade-right" data-aos-duration="2000">
        <h1 class="header">Penyedia Produk Edukasi<br>Muslim TERBAIK di<br>Indonesia</h1>
        <h5>Fokus dalam menghadirkan teknologi yang menjadi solusi dalam belajar dan mengajarkan Al Qur`an, serta menjadi wadah muamalah untuk memperkuat ekonomi umat islam</h5>
    </div>
    <div class="col-6 text-center" data-aos="fade-up" data-aos-duration="3000" ><img src="{{ asset('assets\img\produk.png') }}" style="width: 100%" alt=""></div>
</div>
<div style="width: 100%;" data-aos="fade-up" data-aos-duration="3000"   class="py-5">
    <img style="width: 100%" src="{{ asset('assets\img\visi.jpg') }}" alt="">
</div>
<div class="row py-5">
    <div class="col-12 text-center" data-aos="zoom-in" data-aos-duration="2000" data-aos-delay="1500">
        <h3>Sebelum kami memulai, ada yang harus anda ketahui terlebih dahulu<br>Faktanya Umat Muslim Saat Ini</h3>
    </div>
    <div class="col-4">
        <div class="card my-3" data-aos="fade-up" data-aos-duration="1000" >
            <div class="card-body">
                <img style="width: 100%" class="mb-4" src="{{ asset('assets\img\icon-bacaBaca.png') }}" alt="">
                <h4>85% Belum Bisa Baca Al-Qur'an</h4>
                <p>Khususnya Muslim di Indonesia, dikarenakan berbagai macam masalah dan latar belakang sosial, ekonomi dan budaya.</p>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card my-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="500">
            <div class="card-body">
                <img style="width: 100%" class="mb-4" src="{{ asset('assets\img\icon-maluMalu.png') }}" alt="">
                <h4>65% Malu Belajar Al-Qur'an</h4>
                <p>Beberapa kami temukan karena faktor usia dan lingkungan.</p>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card my-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">
            <div class="card-body">
                <img style="width: 100%" class="mb-4" src="{{ asset('assets\img\icon-produkProduk.png') }}" alt="">
                <h4>Banyak Produk Muslim Tapi Milik Nonmuslim</h4>
                <p>Hal ini menyebabkan konten-konten yang di buat tidak dapat dipertanggungjawabkan, jauh dari edukasi islam dan hanya sekedar transaksional.</p>
            </div>
        </div>
    </div>
    <div class="col-12 pt-5 text-center" >
        <h3 data-aos="zoom-in" data-aos-duration="2000"  >Maka kami,<br>Al Akram Mencoba Memberi Solusi</h3>
        <h6 data-aos="zoom-in" data-aos-duration="2000"  >Berkomitmen untuk memudahkan Umat Islam bisa lebih dekat dengan Al-Qur'an. Membawa Al-Qur'an ke setiap rumah keluarga muslim. Jika belajar membaca masih terkendala guru, waktu, maupun tempat. Maka kini belajar Al-Qur'an bisa dimulai dengan</h6>
        <h3 class="my-4" data-aos="zoom-in" data-aos-duration="2000"  ><i>"Mendengar"</i></h3>
        <h3 style="margin-top:200px " data-aos="zoom-in" data-aos-duration="2000"  >Mengapa dimulai dengan<br>mendengar?</h3>
    </div>
    <div class="col-12"style="margin-bottom:100px " >
        <div class="card" data-aos="fade-up" data-aos-duration="2000" >
            <div class="card-body text-center">
                <div class="mb-4">
                <img src="{{ asset('assets\img\mendengar.png') }}" width="200px" alt="">
                </div>
                <h3 style="margin-top:200px "><b>Ingatkah kita, kapan mulai menghafal Al Fatihah?</b></h3>
                <p>Atau kita tidak sadar bisa hafal Al Fatihah hanya karena sering mengdengarnya berulang sehari hari?Ya, itulah kehebatan indra PENDENGARAN, aktifitas SEDERHANA tapi punya manfaat LUAR BIASA</p>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row py-4" data-aos="zoom-in" data-aos-duration="2000" >
            <div class="col-2"><img style="width: 70%" src="{{ asset('assets\img\produk-image.png') }}" alt="">
            </div>
            <div class="col-10">
                <h3 class="title">
                    PRODUK
                </h3>
                <p>Al-Akram akan terus hadirkan produk dengan inovasi terbaik, demi mengembangkan bisnis dimasa yang akan datang, untuk mendekatkan lebih banyak orang kepada Al-Qur'an</p>
                <a href="/produk">Selengkapnya</a>
            </div>
        </div>
            <div class="row my-4" data-aos="zoom-in" data-aos-duration="2000" >
            <div class="col-2"><img style="width: 70%" src="{{ asset('assets\img\mitra.png') }}" alt="">
            </div>
            <div class="col-10">
                <h3 class="title">
                    KEMITRAAN
                </h3>
                <p>Al-Akram akan terus hadirkan produk dengan inovasi terbaik, demi mengembangkan bisnis dimasa yang akan datang, untuk mendekatkan lebih banyak orang kepada Al-Qur'an</p>
                <a href="https://menghafalitumudah.com/">Selengkapnya</a>
            </div>
        </div>
        <div class="row my-4" data-aos="zoom-in" data-aos-duration="2000" >
            <div class="col-2"><img style="width: 70%" src="{{ asset('assets\img\program.png') }}" alt="">
            </div>
            <div class="col-10">
                <h3 class="title">
                    PROGRAM
                </h3>
                <p>Al-Akram akan terus hadirkan produk dengan inovasi terbaik, demi mengembangkan bisnis dimasa yang akan datang, untuk mendekatkan lebih banyak orang kepada Al-Qur'an</p>
                <a href="https://akramofficial.com/jadi-mitra-al-akram/">Selengkapnya</a>
            </div>
        </div>

    </div>
</div>
@endsection

@push('js')

@endpush
