@extends('layouts.app')
@section('content')
<div class="container">
    <a href="/produk" class="btn btn-primary mb-4">Kembali</a>
    <div class="gambar">
        @if ($produk->gambar)
        <img src="/storage/{{ $produk->gambar }}" width="100%"  alt="">
        @else
        <img style="width: 100%" src="{{ asset('assets\img\img\backgrounds\18.jpg') }}" alt="">
        @endif
    </div>
    <div class="content mt-4">
        <h1>{{ $produk->nama }}</h1>
        <h4>Rp {{ number_format($produk->harga, 2); }}</h4>
        {!! $produk->deskripsi !!}

        <a target="blank" href="https://wa.me/6289689339929" class="btn btn-success">Hubungi Kami</a>
    </div>
</div>
@endsection

@push('js')

@endpush
