@extends('layouts.admin')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-xl-12 col-xxl-12 d-flex">
        <div class="w-100">
          <div class="row">
            <div class="col-4">
              <div class="card">
                <h5 class="card-title pt-3 px-3">Penjualan</h5>
                <div class="card-body d-flex">
                    <div class="align-self-center w-100">
                      <div class="py-3">
                        <div class="chart chart-xs">

                          <canvas id="chartjs-pj"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Penjualan</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-omset"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Youtube</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-yt"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Facebook</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-fb"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Akram Ads</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-akrm"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Instagram</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-ig"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                  <h5 class="card-title pt-3 px-3">Omset Market Place</h5>
                  <div class="card-body d-flex">
                      <div class="align-self-center w-100">
                        <div class="py-3">
                          <div class="chart chart-xs">

                            <canvas id="chartjs-market"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      // Pie chart
      new Chart(document.getElementById("chartjs-pj"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $pj_bulan_lalu }},
            {{ $pj_bulan }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-omset"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_bulan_lalu }},
            {{ $omset_bulan }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-yt"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_youtube_lalu }},
            {{ $omset_youtube }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-fb"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_facebook_lalu }},
            {{ $omset_facebook }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-akrm"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_akram_lalu }},
            {{ $omset_akram }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-ig"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_instagram_lalu }},
            {{ $omset_instagram }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
      new Chart(document.getElementById("chartjs-market"), {
        type: "bar",
        data: {
          labels: [
           'Bulan Lalu',
           'Bulan Ini'
          ],
          datasets: [{
            data: [

            {{ $omset_market_lalu }},
            {{ $omset_market }}
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                    if(parseInt(value) >= 1000){
                        return 'Rp ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    } else {
                        return 'Rp ' + value;
                    }
                    }
                }
                }]
            }

        }
      });
    });




    document.addEventListener("DOMContentLoaded", function() {
      // Line chart
      new Chart(document.getElementById("chartjs-line"), {
        type: "line",
        data: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [
            @foreach ($perjualanPerBulan as $key => $item)
              {
                label: "{!! $key !!}",
                fill: true,
                backgroundColor: "transparent",
                borderColor: "#" + Math.floor(Math.random() * 16777215).toString(16),
                data: [
                  @foreach ($item as $key => $val)
                    '{{ $val[1] }}',
                  @endforeach
                ]
              },
            @endforeach
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: true
          },
          tooltips: {
            intersect: false
          },
          hover: {
            intersect: true
          },
          plugins: {
            filler: {
              propagate: false
            }
          },
          scales: {
            xAxes: [{
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.05)"
              }
            }],
            yAxes: [{
              ticks: {
                stepSize: 500
              },
              display: true,
              borderDash: [5, 5],
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff"
              }
            }]
          }
        }
      });
    });
  </script>
@endpush
