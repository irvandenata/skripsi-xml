@extends('layouts.admin')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
  <div class="content">
    <div class="row">
      <div class="col-xl-12 col-xxl-12 d-flex">
        <div class="w-100">
          <div class="row">
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Penjualan<br>Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="truck"></i>
                      </div>
                    </div>
                  </div>
                  <h1 class="mt-1 mb-3">{{ $pj_bulan }}</h1>
                  <div class="mb-0">
                    <span class="{{ $pj_bulan > $pj_bulan_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                      {{ $pj_bulan > $pj_bulan_lalu ? '+' : '' }}{{ $pj_bulan_lalu != 0 ? $pj_bulan - $pj_bulan_lalu : $pj_bulan }}</span>
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Produk</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="package"></i>
                      </div>
                    </div>
                  </div>
                  <h1 class="mt-1 mb-3">{{ $produk }}</h1>
                  <div class="mb-0">
                    <span class="text-muted">Jenis</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Keseluruhan Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_bulan, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_bulan > $omset_bulan_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                        @if ($omset_bulan_lalu != 0)
                        {{ $omset_bulan > $omset_bulan_lalu ? '+' : '' }}{{ round((($omset_bulan - $omset_bulan_lalu) / $omset_bulan_lalu) * 100, 2) }}%</span>
                        @else
                        -100%</span>
                        @endif

                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Youtube Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_youtube, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_youtube > $omset_youtube_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                    @if($omset_youtube_lalu !=0)
                      {{ $omset_youtube > $omset_youtube_lalu ? '+' : '' }}{{ round((($omset_youtube - $omset_youtube_lalu) / $omset_youtube_lalu) * 100, 2) }}%</span>
                      @else
                      -100%</span>
                      @endif
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Facebook Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_facebook, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_facebook > $omset_facebook_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                        @if($omset_facebook_lalu !=0)
                        {{ $omset_facebook > $omset_facebook_lalu ? '+' : '' }}{{ round((($omset_facebook - $omset_facebook_lalu) / $omset_facebook_lalu) * 100, 2) }}%</span>
                        @else
                        -100%</span>
                        @endif
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Akram Ads Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_akram, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_akram > $omset_akram_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                        @if($omset_akram_lalu !=0)
                        {{ $omset_akram > $omset_akram_lalu ? '+' : '' }}{{ round((($omset_akram - $omset_akram_lalu) / $omset_akram_lalu) * 100, 2) }}%</span>
                        @else
                        -100%</span>
                        @endif
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Instagram Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_instagram, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_instagram > $omset_instagram_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                        @if($omset_instagram_lalu !=0)
                        {{ $omset_instagram > $omset_instagram_lalu ? '+' : '' }}{{ round((($omset_instagram - $omset_instagram_lalu) / $omset_instagram_lalu) * 100, 2) }}%</span>
                        @else
                        -100%</span>
                        @endif
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col mt-0">
                      <h5 class="card-title">Omset Market Place Bulan Ini</h5>
                    </div>
                    <div class="col-auto">
                      <div class="stat text-primary">
                        <i class="align-middle" data-feather="dollar-sign"></i>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-1 mb-3">{{ 'Rp ' . number_format($omset_market, 2, ',', '.') }}</h4>
                  <div class="mb-0">
                    <span class="{{ $omset_market > $omset_market_lalu ? 'text-success' : 'text-danger' }}"> <i
                        class="mdi mdi-arrow-bottom-right"></i>
                        @if($omset_market_lalu !=0)
                        {{ $omset_market > $omset_market_lalu ? '+' : '' }}{{ round((($omset_market - $omset_market_lalu) / $omset_market_lalu) * 100, 2) }}%</span>
                        @else
                        -100%</span>
                        @endif
                    <span class="text-muted">Dibanding Bulan Lalu</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12">
        <div class="card flex-fill w-100">
          <div class="card-header">
            <h5 class="card-title mb-0">Produk Paling Banyak Terjual<br>Bulan Ini </h5>
          </div>
          <div class="card-body d-flex">
            <div class="align-self-center w-100">
              <div class="py-3">
                <div class="chart chart-xs">
                  <canvas id="chartjs-dashboard-pie"></canvas>
                </div>
              </div>
              <table class="table mb-0">
                <tbody>
                  @foreach ($produkTerbanyak as $item)
                    <tr>
                      <td>{{ $item->nama }}</td>
                      <td class="text-end">{{ $item->total_jual }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-12 col-lg-12 col-xxl-12 d-flex">
        <div class="card flex-fill">
          <div class="card-header">

            <h5 class="card-title mb-0">Penjualan</h5>
          </div>
          <div class="card-body">
            <div class="chart">
              <div class="chartjs-size-monitor">
                <div class="chartjs-size-monitor-expand">
                  <div class=""></div>
                </div>
                <div class="chartjs-size-monitor-shrink">
                  <div class=""></div>
                </div>
              </div>
              <canvas id="chartjs-line" style="display: block; width: 445px; height: 300px;" width="445"
                height="300" class="chartjs-render-monitor"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      // Pie chart
      new Chart(document.getElementById("chartjs-dashboard-pie"), {
        type: "pie",
        data: {
          labels: [
            @foreach ($produkTerbanyak as $item)
              "{!! $item->nama !!}",
            @endforeach
          ],
          datasets: [{
            data: [
              @foreach ($produkTerbanyak as $item)
                "{{ $item->total_jual }}",
              @endforeach
            ],
            backgroundColor: [
              window.theme.primary,
              window.theme.warning,
              window.theme.danger,
              window.theme.black,
              window.theme.success,
              window.theme.secondary,
            ],
            borderWidth: 5
          }]
        },
        options: {
          responsive: !window.MSInputMethodContext,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          cutoutPercentage: 75
        }
      });
    });




    document.addEventListener("DOMContentLoaded", function() {
      // Line chart
      new Chart(document.getElementById("chartjs-line"), {
        type: "line",
        data: {
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          datasets: [
            @foreach ($perjualanPerBulan as $key => $item)
              {
                label: "{!! $key !!}",
                fill: true,
                backgroundColor: "transparent",
                borderColor: "#" + Math.floor(Math.random() * 16777215).toString(16),
                data: [
                  @foreach ($item as $key => $val)
                    '{{ $val[1] }}',
                  @endforeach
                ]
              },
            @endforeach
          ]
        },
        options: {
          maintainAspectRatio: true,
          legend: {
            display: true
          },
          tooltips: {
            intersect: false
          },
          hover: {
            intersect: true
          },
          plugins: {
            filler: {
              propagate: false
            }
          },
          scales: {
            xAxes: [{
              reverse: true,
              gridLines: {
                color: "rgba(0,0,0,0.05)"
              }
            }],
            yAxes: [{
              ticks: {
                stepSize: 500
              },
              display: true,
              borderDash: [5, 5],
              gridLines: {
                color: "rgba(0,0,0,0)",
                fontColor: "#fff"
              }
            }]
          }
        }
      });
    });
  </script>
@endpush
