<!DOCTYPE html>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

  <!-- Scripts -->
  {{-- @vite(['resources/sass/app.scss', 'resources/js/app.js']) --}}
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
  <meta name="author" content="Akram Official">
  <meta name="keywords"
    content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link rel="shortcut icon" href="{{ asset('assets') }}/img/icons/icon-48x48.png" />
  <link rel="canonical" href="https://demo-basic.adminkit.io/" />
  <link href="{{ asset('assets') }}/css/app.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"
    integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
    .modal {
      top: 50% !important;
    }

    .swal-wide {
      width: auto !important;
      height: 90vh !important;
    }

    .spiner {
      position: absolute;
      width: 100vw;
      height: 100vh;
      background-color: rgb(255, 255, 255) !important;
      z-index: 1000 !important;
    }

    .toast-error {
      background-color: brown !important;
    }

    .toast-success {
      background-color: green !important;
    }

    .overflow-hidden {
      overflow: hidden;
    }

    .absolute {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50, -50);
    }

    .scale-2 {
      scale: 2;
    }

    .hide {
      display: none;
    }

    .modal {
      transform: translateY(-50%) !important;
    }
  </style>
  @stack('css')
  @stack('style')
</head>

<body class="overflow-hidden">
  <div class="spiner">
    <div class="spinner-border text-primary absolute scale-2" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>
  <div class="wrapper ">
    <nav id="sidebar" class="sidebar js-sidebar">
      <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="index.html">
          <span class="align-middle">Akram Official</span>
        </a>

        <ul class="sidebar-nav">
          <li class="sidebar-header">
            Pages
          </li>

          <li class="sidebar-item @if (Request::is('admin/dashboard*')) active @endif">
            <a class="sidebar-link" href="{{ route('admin.dashboard.index') }}">
              <i class="align-middle" data-feather="activity"></i> <span class="align-middle">Dashboard</span>
            </a>
          </li>
          <li class="sidebar-item @if (Request::is('admin/penjualans*')) active @endif">
            <a class="sidebar-link" href="{{ route('admin.penjualans.index') }}">
              <i class="align-middle" data-feather="dollar-sign"></i> <span class="align-middle">Penjualan</span>
            </a>
          </li>
          @if (auth()->user()->is_admin == 1)

          <li class="sidebar-item @if (Request::is('admin/produks*')) active @endif">
            <a class="sidebar-link" href="{{ route('admin.produks.index') }}">
              <i class="align-middle" data-feather="package"></i> <span class="align-middle">Produk</span>
            </a>
          </li>
          <li class="sidebar-item @if (Request::is('admin/settings*')) active @endif">
            <a class="sidebar-link" href="{{ route('admin.settings.index') }}">
              <i class="align-middle" data-feather="settings"></i> <span class="align-middle">Setting</span>
            </a>
          </li>
          <li class="sidebar-item @if (Request::is('admin/users*')) active @endif">
            <a class="sidebar-link" href="{{ route('admin.users.index') }}">
              <i class="align-middle" data-feather="users"></i><span class="align-middle">Pengguna</span>
            </a>
          </li>

          @endif
          <li class="sidebar-item @if (Request::is('admin/grafiks*')) active @endif">
            <a class="sidebar-link" href="/admin/grafiks">
              <i class="align-middle" data-feather="activity"></i><span class="align-middle">Grafik</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="main">
        <nav class="navbar navbar-expand navbar-light navbar-bg">
            <a class="sidebar-toggle js-sidebar-toggle m-3">
              <i class="hamburger align-self-center"></i>
            </a>

            <div class="navbar-collapse collapse">
              <ul class="navbar-nav navbar-align">
                {{-- <li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                    <div class="position-relative">
                      <i class="align-middle" data-feather="bell"></i>
                      <span class="indicator">{{ auth()->user()->unreadNotifications->count() }}</span>
                    </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0" aria-labelledby="alertsDropdown">
                    <div class="dropdown-menu-header">
                      {{ auth()->user()->unreadNotifications->count() }} New Notifications
                    </div>

                    <div class="list-group">
                     @foreach ( auth()->user()->unreadNotifications as $notification )
                     <a href="{{ route('admin.complaints.show',$notification->data['id']) }}?notifId={{ $notification->id }}" style="text-decoration: none" class="list-group-item notif" data-id="{{ $notification }}">
                        <div class="row g-0 align-items-center">
                          <div class="col-2">
                            <i class="text-warning" data-feather="bell"></i>
                          </div>
                          <div class="col-10">
                            <div class="text-dark">Complaint</div>
                            <div class="text-muted small mt-1">New Complaint ! Click to check</div>
                            <div class="text-muted small mt-1">{{ $notification->created_at->diffForHumans()  }}</div>
                          </div>
                        </div>
                      </a>
                     @endforeach
                    </div>
                  </div>
                </li> --}}
                <li class="nav-item dropdown">
                  <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                    <i class="align-middle" data-feather="settings"></i>
                  </a>

                  <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
                    <img src="{{ asset('assets/img/avatars/avatar.jpg') }}" class="avatar img-fluid rounded me-1" alt="Charles Hall" /> <span
                      class="text-dark">{{ auth()->user()->name }}</span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-end" style="width: 200px !important">
                    <a class="dropdown-item" onclick="showProfile()"><i class="align-middle me-1"
                        data-feather="user"></i> Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                     {{ __('Logout') }}
                 </a>

                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
      <main class="content p-4 m-2">
        <div class="container-fluid p-0">
          <h1 class="h3 mb-3">{!! $breadcrumb !!}</h1>
          @yield('content')
        </div>
      </main>

      <footer class="footer">
        <div class="container-fluid">
          <div class="row text-muted">
            <div class="col-6 text-start">
              <p class="mb-0">
                <a class="text-muted" href="https://adminkit.io/" target="_blank"><strong>Akram Official</strong></a>
                &copy;
              </p>
            </div>
            <div class="col-6 text-end">
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a class="text-muted" href="https://adminkit.io/" target="_blank">Support</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="https://adminkit.io/" target="_blank">Help Center</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="https://adminkit.io/" target="_blank">Privacy</a>
                </li>
                <li class="list-inline-item">
                  <a class="text-muted" href="https://adminkit.io/" target="_blank">Terms</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="modal fade " id="modalProfile" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered @yield('modal-size')" role="document">
        <div class="modal-content">
                <input id="id" type="hidden" name="id" value="">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalProfileTitle">Profile</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group mb-1 col-12">
                            <div class="form-line">
                              <label for="name">Name</label>
                              <input type="text" name="name" value="{{ auth()->user()->name }}" class="form-control name" required>
                            </div>
                          </div>
                        <div class="form-group mb-1 col-12">
                          <div class="form-line">
                            <label for="name">Email</label>
                            <input type="email" name="email" value="{{ auth()->user()->email }}" class="form-control email" required>
                          </div>
                        </div>
                        <div class="form-group mb-1 col-12">
                            <div class="form-line">
                              <label for="name">Password</label>
                              <input type="password" name="password" minlength="6" class="form-control password" required>
                            </div>
                          </div>
                          <div class="form-group mb-1 col-12">
                            <div class="form-line">
                              <label for="name">Confirm Password</label>
                              <input type="password"  class="form-control password" minlength="6" required>
                              <small class="valid text-danger"></small>
                            </div>
                          </div>
                      </div>
                </div>
                <div class="modal-footer">
                    <button  class="btn waves-effect btn-primary" onclick="sendUpdate()">Save</button>
                    <button type="button" class="btn waves-effect btn-danger close-modal"  onclick="closeModal()"  data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>
 </div>

  <script src="{{ asset('assets') }}/js/app.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://code.jquery.com/jquery-3.6.3.min.js"
    integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/js/all.min.js"
    integrity="sha512-rpLlll167T5LJHwp0waJCh3ZRf7pO6IT1+LZOhAyP6phAirwchClbTZV3iqL3BMrVxIYRbzGTpli4rfxsCK6Vw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>


  <script>
    $(window).on('load', function() {
      $('body').removeClass('overflow-hidden');
      $('.spiner').addClass('fade')
      $('.spiner').addClass('hide')
      $('.form-control-sm').attr('autocomplete', 'off');

    })

    function preview(e) {
      if (e.files[0]) {

        var reader = new FileReader();
        reader.onload = function(e) {
          $('#preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(e.files[0]);
      }
    }

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    $('.select2').select2({
      placeholder: 'Select an option',
      allowClear: true,
      width: 'resolve',
      height: 'resolve'
    });



    @if (session('success'))
      Toast.fire({
        icon: 'success',
        title: '{{ session('success') }}'
      })
    @endif

    function toast() {
      toastr.error('value', 'Error', {
        closeButton: true,
        progressBar: true,
        timeOut: 5000000
      });
    }
  </script>
  <script>
    @if (Session::get('error'))

      @foreach (session('error') as $error)
        toastr.error('{{ $error }}', 'Error', {
          closeButton: true,
          progressBar: true,
        });
      @endforeach
    @endif
    function showImage(item) {
      Swal.fire({
        title: 'Image Preview',
        imageUrl: $(item).attr('src'),
        imageAlt: 'Image Preview',
        customClass: 'swal-wide',
        confirmButtonText: 'Close',
      })
    }



    function showProfile(){
        $('#modalProfile').modal('show')
     }

     function sendUpdate(e){
        let password = $('.password').toArray();
        console.log(password)
        let one = '';
        let condition = false;
        $('.valid').empty()

        password.forEach((element,index) => {
            if(index==0){
                one = $(element).val()
            }else{
                if(one != $(element).val() || one == '' || $(element).val() == ''){
                    $('.valid').append('Password not match !')
                }else{
                    condition = true;
                }
            }
        });
        if(condition){
            $.ajax({
            url: '/admin/send/profile',
            type: "post",
            cache: false,
            data: {
                'name' : $('.name').val(),
                'email' : $('.email').val(),
                'password' : $('.password').val(),
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                $('#modalProfile').modal('hide')
                Toast.fire({
                    icon: 'success',
                    title: result.message
                })
                // toastr.success('Berhasil Disimpan', 'Success');
            },
            error: async function (result) {
                Swal.fire({
                    icon: 'error',
                    text: 'Terjadi Kesalahan !',
                    showCancelButton: false,
                }).then(() => {
                    if (result.responseJSON) {
                        console.log(result.responseJSON)
                        getError(result.responseJSON);
                    } else {
                        console.log(result);
                    }
                })
              },
            })
        }

     }
  </script>

  @stack('js')
  @stack('script')


</body>

</html>
