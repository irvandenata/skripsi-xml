@extends('layouts.admin')

@section('title', $title)
@section('breadcrumb', $breadcrumb)

@push('css')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.min.css"
    integrity="sha512-888I2nScRzrb/WNZ3qsa5kiZNmaEsvz8HEVQZRGokIEOj/sMnUlLClqP7itKJYDhXWsmp+1usxoCidSEfW2rWw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('style')
@endpush

@section('content')
  <div class="row">
    <div class="col-xl-12 col-xxl-12 ">
      <div class="w-100">
        <div class="row">
          <div class="col-12">
            <div class="card p-4">
              <form action="{{ $updateLink }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                  <div class="form-group mb-3 col-6">
                    <div class="form-line">
                      <label for="name">Name</label>
                      <input type="text" name="name" value="{{ $item->name }}" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group mb-3 col-6">
                    <div class="form-line">
                      <label for="name">Type</label>
                      <input type="text" value="{{ $item->type }}" name="type" class="form-control">
                    </div>
                  </div>
                  <div class="form-group mb-3 col-6 ">
                    <div class="form-line">
                      <label for="name">Category</label>
                      <select name="category_id" class="form-control select-category" required>
                        <option value="{{ $item->category->id }}" selected>{{ $item->category->name }}</option>
                      </select>
                    </div>
                    <div class="btn btn-primary mt-2 showModalCategory">Create New Category</div>
                  </div>
                  <div class="form-group mb-3 col-6">
                    <div class="form-line">
                      <label for="name">Brand</label>
                      <select name="brand_id" class="form-control select-brand" required>
                        <option value="{{ $item->brand->id }}" selected>{{ $item->brand->name }}</option>
                      </select>
                    </div>
                    <div class="btn btn-primary mt-2 showModalBrand">Create New Brand</div>
                  </div>
                </div>
                <div>
                  <a href="{{ $indexLink }}" class="btn btn-secondary mt-2">Back</a>
                  <button type="submit" class="btn btn-success mt-2" id="submit">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <form method="POST">
          <input id="id" type="hidden" name="id" value="">
          <div class="modal-header">
            <h4 class="modal-title" id="modalFormTitle">Create Category</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control nameCategory" required>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn  waves-effect btn-primary" id='createCategory'>Save</button>
            <button type="button" class="btn  waves-effect btn-danger close-modal" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalBrand" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <form method="POST">
          <input id="id" type="hidden" name="id" value="">
          <div class="modal-header">
            <h4 class="modal-title" id="modalFormTitle">Create Brand</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control nameBrand" required>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn  waves-effect btn-primary" id='createBrand'>Save</button>
            <button type="button" class="btn waves-effect btn-danger close-modal" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@push('js')
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modal.min.js"
    integrity="sha512-0wCoO9w07Mu4MnC918HEsFyXhVJVoxeq+RD4XXYukmLswUHMCRbBomZE+NjxBtv88QTU/fImTY+PclhlMpJ4JA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endpush

@push('script')
  <script>
    function deleteItem(id) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true,
      })

      swalWithBootstrapButtons.fire({
        title: 'Are You Sure ?',
        text: "You will delete this data!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Delete!',
        cancelButtonText: 'No, Quit!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          deleteData(id).then((result) => {

            if (result) {

              swalWithBootstrapButtons.fire(
                'Deleted!',
                'Data Has Been Deleted',
                'success'
              ).then(() => {
                limitOrder--;

                reloadDatatable();

              })
            }
          });


        } else if (
          // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancel',
            'Process Has Been Canceled',
            'error'
          )
        }
      })
    }

    /** hapus data dari database **/
    function deleteData(id) {

      return new Promise((resolve, reject) => {
        var url = child_url + '/' + id;
        let result = false;
        Swal.fire({
          text: 'Please wait.',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonText: "ok",
          allowOutsideClick: false,
          allowEscapeKey: false
        })
        Swal.showLoading()
        $.ajax({
          url: url,
          type: "DELETE",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(result) {
            resolve(true)
            // toastr.success('Berhasil Dihapus', 'Success');
          },
          error: function(errors) {

            getError(errors.responseJSON);
            Swal.fire({
              icon: 'error',
              text: 'Terjadi Kesalahan !',
              showCancelButton: false,
            })
            reject(false)
          }
        })
      })

    }
  </script>
  <script>
    $('.select-category').select2({
      ajax: {
        url: "/api/get-categories-equipment",
        data: function(params) {
          var data = {
            data: params.term
          }
          return data;
        },
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.name,
                id: item.id,
              }
            })
          }
        }
      },
      placeholder: "--- Select an Option ---",
      allowClear: true
    });
    $('.select-category').trigger('change')
    $('.select-brand').select2({
      ajax: {
        url: "/api/get-brands-equipment",
        data: function(params) {
          var data = {
            data: params.term
          }
          return data;
        },
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.name,
                id: item.id,
              }
            })
          }
        }
      },
      placeholder: "--- Select an Option ---",
      allowClear: true
    });
    $('.select-brand').trigger('change')


    @if (session('error'))
      @foreach (session('error') as $error)
        Toast.fire({
          icon: 'error',
          name: '{{ $error }}'
        })
      @endforeach
    @endif
  </script>

  <script>
    $('.close-modal').on('click', function() {
      $('.modal').modal('hide')

    })
    $('#createCategory').on('click', function(e) {
      if ($('.nameCategory').val() != '') {
        e.preventDefault();
        $.ajax({
          url: '/api/create-category',
          type: "post",
          cache: false,
          dataType: 'json',
          data: new FormData($('#modalCategory form')[0]),
          contentType: false,
          processData: false,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(result) {
            Toast.fire({
              icon: 'success',
              title: result.message
            })
            $('.modal').modal('hide')

            // toastr.success('Berhasil Disimpan', 'Success');
          },
          error: async function(result) {
            Swal.fire({
              icon: 'error',
              text: 'Terjadi Kesalahan !',
              showCancelButton: false,
            }).then(() => {
              if (result.responseJSON) {
                console.log(result.responseJSON)
                getError(result.responseJSON);
              } else {
                console.log(result);
              }
            })

          },
        })
      }
    })

    $('#createBrand').on('click', function(e) {
      if ($('.nameBrand').val() != '') {
        e.preventDefault();
        $.ajax({
          url: '/api/create-brand',
          type: "post",
          cache: false,
          dataType: 'json',
          data: new FormData($('#modalBrand form')[0]),
          contentType: false,
          processData: false,
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(result) {
            Toast.fire({
              icon: 'success',
              title: result.message
            })
            $('.modal').modal('hide')
            // toastr.success('Berhasil Disimpan', 'Success');
          },
          error: async function(result) {
            Swal.fire({
              icon: 'error',
              text: 'Terjadi Kesalahan !',
              showCancelButton: false,
            }).then(() => {
              if (result.responseJSON) {
                console.log(result.responseJSON)
                getError(result.responseJSON);
              } else {
                console.log(result);
              }
            })

          },
        })
      }
    })

    $('.showModalCategory').on('click', function() {
      $('#modalCategory').modal('show');
      $('#modalCategory form')[0].reset();

    })
    $('.showModalBrand').on('click', function() {
      $('#modalBrand').modal('show');
      $('#modalBrand form')[0].reset();

    })
  </script>
@endpush
