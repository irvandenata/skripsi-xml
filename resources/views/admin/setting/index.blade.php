@extends('layouts.admin')

@section('title', $title)
@section('breadcrumb', $breadcrumb)

@push('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endpush

@push('style')
  <style>
    #datatable_filter {
      margin-bottom: 10px !important;
    }
  </style>
@endpush

@section('content')
  <div class="row">
    <div class="col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
      <div class="card p-4">
        <form id="formCrud" action="{{ route('admin.settings.store') }}" method="POST">
          @csrf
          <input id="id" type="hidden" name="id" value="">
          <div class="modal-body">
            <div class="row">
                <div class="form-group mb-1 col-12">
                  <div class="form-line">
                    <label for="name" class="mb-2">LINK API SHEET</label>
                    <input type="text" name="link" class="form-control" value="{{ $link_api->config_value }}" placeholder="Masukkan LINK API SHEAT" required>
                  </div>
                </div>
            <div class="row">
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Youtube</label>
                  <input type="text" name="sheat_youtube" class="form-control" value="{{ $sheat_youtube->config_value }}" placeholder="Masukkan ID Sheet Youtube" required>
                </div>
              </div>
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Ads Akram</label>
                  <input type="text" name="sheat_ads"  value="{{ $sheat_ads->config_value }}" class="form-control" placeholder="ID Sheet Ads akram" required>
                </div>
              </div>
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Instagram</label>
                  <input type="text" name="sheat_instagram"  value="{{ $sheat_instagram->config_value }}" class="form-control" placeholder="ID Sheet Instagram" required>
                </div>
              </div>
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Facebook</label>
                  <input type="text" name="sheat_facebook" class="form-control" placeholder="ID Sheet Facebook"  value="{{ $sheat_facebook->config_value }}" required>
                </div>
              </div>
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Market Place</label>
                  <input type="text" name="sheat_market" class="form-control" placeholder="ID Sheet Market"  value="{{ $sheat_market->config_value }}" required>
                </div>
              </div>
              <div class="form-group mb-1 col-12">
                <div class="form-line">
                  <label for="name" class="mb-2">ID Sheet Produk</label>
                  <input type="text" name="sheat_produk" class="form-control" placeholder="ID Sheet Produk"  value="{{ $sheat_produk->config_value }}" required>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer mt-2">
            <button type="submit" class="btn waves-effect btn-primary" id='submit'>Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush

@push('script')
@endpush
