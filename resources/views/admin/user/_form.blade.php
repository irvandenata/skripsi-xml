@extends('utils.modal')
@section('modal-size', 'modal-lg')
@section('input-form')
  <div class="row">
    <div class="form-group mb-1 col-12">
      <div class="form-line">
        <label for="name">Name</label>
        <input type="text" name="name" value="" class="form-control name" required>
      </div>
    </div>
    <div class="form-group mb-1 col-12">
      <div class="form-line">
        <label for="name">Email</label>
        <input type="email" name="email" value="" class="form-control email" required>
      </div>
    </div>
    <div class="form-group mb-1 col-12">
      <div class="form-line">
        <label for="name">Password</label>
        <input type="password" name="password" minlength="6" class="form-control password" required>
      </div>
    </div>
  </div>
@endsection
