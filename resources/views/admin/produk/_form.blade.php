@extends('utils.modal')
@section('modal-size', 'modal-lg')
@section('input-form')
  <div class="row">
    <div class="form-group mb-1 col-12">
      <div class="form-line">
        <label for="name">Kode</label>
        <input type="text" name="kode" class="form-control" required>
      </div>
    </div>
    <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Nama</label>
          <input type="text" name="nama" class="form-control" required>
        </div>
    </div>
    <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Gambar</label>
          <input type="file" name="gambar" class="form-control" >
        </div>
    </div>

    <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Stok</label>
          <input type="number" name="stok" class="form-control" required>
        </div>
    </div>
    <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Deskripsi</label>
          <textarea id="tiny" name="deskripsi" class="form-control"></textarea>
        </div>
    </div>
  </div>
@endsection
