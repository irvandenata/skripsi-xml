@extends('utils.modal')
@section('modal-size', 'modal-md')
@section('input-form')
  <div class="row">
    <div class="form-group mb-1 col-12">
      <div class="form-line">
        <label for="name">Produk</label>
        <select name="produk_id" class="form-control" id="" required>
            <option value="" disabled selected>--- Pilih Produk ---</option>
            @foreach ($produks as $item)
              <option value="{{ $item->id }}" >{{ $item->nama }} ({{ $item->kode }})</option>
            @endforeach
        </select>
      </div>
    </div>
    <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Kode Pembelian</label>
          <input type="text" name="kode" class="form-control" required>
        </div>
      </div>
      <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Nama Pembeli</label>
          <input type="text" name="pembeli" class="form-control" required>
        </div>
      </div>
      <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Sumber</label>
          <select name="sumber" class="form-control" id="" required>
            <option value="" disabled selected>--- Pilih Sumber ---</option>
              <option value="Facebook" >Facebook</option>
              <option value="Youtube" >Youtube</option>
              <option value="Akram Ads" >Akram Ads</option>
        </select>
        </div>
      </div>
      <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Jumlah</label>
          <input type="number" name="jumlah" class="form-control" required>
        </div>
      </div>
      <div class="form-group mb-1 col-12">
        <div class="form-line">
          <label for="name">Tanggal Pembelian</label>
          <input type="date" name="tanggal" class="form-control" required>
        </div>
      </div>
  </div>
@endsection
